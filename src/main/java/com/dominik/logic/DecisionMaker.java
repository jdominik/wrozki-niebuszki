package com.dominik.logic;

import com.dominik.db.RetrieveData;
import com.dominik.db.State;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;


public class DecisionMaker {
    public static final Long SHOOT = 1L;
    public static final Long RELOAD = 2L;
    public static final Long BLOCK = 3L;

    public static final Long MY_OFFENSIVE_STRATEGY = 1L;
    public static final Long MY_SUSTAINABLE_STRATEGY = 2L;
    public static final Long MY_DEFENSIVE_STRATEGY = 3L;
    public static final Long OPP_OFFENSIVE_STRATEGY = 4L;
    public static final Long OPP_SUSTAINABLE_STRATEGY = 5L;
    public static final Long OPP_DEFENSIVE_STRATEGY = 6L;
    public static final Long OPP_ALTERNATING_STRATEGY = 7L;
    public static final Long OPP_IMPRESIVE_STRATEGY = 8L;
    public static final Long OPP_DUMMY_STRATEGY = 9L;

    private static final Long RANDOM_1 = 1L;


    public String takeAction(Long decision) {

        switch (decision.intValue()) {
            case 1 : return "shoot";
            case 2 : return "reload";
            case 3 : return "block";
            default: return "block";
        }
    }

    public Long makeDecision(State myState, State oppState, Long myStrategy, Long oppStrategy) {
        if (myState.getLoaded()) {
            if (oppState.getLoaded()) {
                if (myState.getBlockAsFirst()) {
                    return blockAsFirstResolution(myState,myStrategy);
                } else {
                    return defeatOpponentResolution(myState,oppState,myStrategy,oppStrategy);
                }
            } else {
                return detectOppStrategyAndShoot(myState, oppState, myStrategy, oppStrategy);
            }
        } else {
            return RELOAD;
        }
    }
    private Long detectOppStrategyAndShoot(State myState, State oppState, Long myStrategy, Long oppStrategy) {
        Long randomDecision = (long) Math.random();
        if (OPP_DEFENSIVE_STRATEGY.equals(oppStrategy)) {
            if (oppState.getThirdBlock() || (!RANDOM_1.equals(randomDecision) && oppState.getSecondBlock()) || myState.getThirdBlock()) {
                return SHOOT;
            } else {
                return BLOCK;
            }
        } else if (OPP_OFFENSIVE_STRATEGY.equals(oppStrategy)) {
            return SHOOT;
        } else if (!RANDOM_1.equals(randomDecision) || (!RANDOM_1.equals(randomDecision) && oppState.getFirstBlock()) || oppState.getSecondBlock()) {
            return SHOOT;
        } else if (!myState.getThirdBlock()) {
            return BLOCK;
        } else return RELOAD;   // just in case
    }

    private Long blockAsFirstResolution(State myState, Long myStrategy) {
        if (MY_DEFENSIVE_STRATEGY.equals(myStrategy)) {
            if (myState.getSecondBlock()) {
                return SHOOT;
            } else {
                return BLOCK;
            }
        } else if (MY_SUSTAINABLE_STRATEGY.equals(myStrategy)) {
            if (myState.getFirstBlock()) {
                return SHOOT;
            } else {
                return BLOCK;
            }
        } else {    // offensive
            return SHOOT;
        }
    }
    private Long defeatOpponentResolution(State myState, State oppState, Long myStrategy, Long oppStrategy) {
        Long randomDecision = (long) Math.random();
        if (OPP_IMPRESIVE_STRATEGY.equals(oppStrategy) && RANDOM_1.equals(randomDecision)) {
            return reloadForInsaneResolution(myState, myStrategy);
        } else if (OPP_DUMMY_STRATEGY.equals(oppStrategy) && RANDOM_1.equals(randomDecision)) {
            return SHOOT;
        } else {
            if (oppState.getThirdBlock() || myState.getThirdBlock()) {
                return SHOOT;
            } else {
                return BLOCK;
            }
        }
    }

    private Long reloadForInsaneResolution(State myState, Long myStrategy) {
        if (MY_DEFENSIVE_STRATEGY.equals(myStrategy)) {
            if (myState.getSecondBlock()) {
                return ((long) Math.random()) + 1L; // reload or shoot
            } else {
                return BLOCK;
            }
        } else if (MY_SUSTAINABLE_STRATEGY.equals(myStrategy)) {
            if (myState.getFirstBlock()) {
                return ((long) Math.random()) + 1L; // reload or shoot
            } else {
                return BLOCK;
            }
        } else {    // offensive
            return ((long) Math.random()) + 1L; // reload or shoot
        }
    }

    public Pair defineState(Long oppStateRecordId, Long myStateRecordId, Long oppMove, Long myMove, Long detectedStrategy) {
        RetrieveData data = new RetrieveData();
        State oppState = data.retrieveState(oppStateRecordId);
        State myState = data.retrieveState(myStateRecordId);
        //Long myStrategy = null;
        boolean isMy = oppState.getMyState();
        if (!RELOAD.equals(oppMove) && !oppState.getLoaded() && !myState.getLoaded()) {
            if (!isMy && oppState.getNotReloadWhenDoubleEmpty()) {
                detectedStrategy = OPP_ALTERNATING_STRATEGY;
            }
            oppState.setNotReloadWhenDoubleEmpty(true);
        }
        if (!BLOCK.equals(oppMove)) {
            oppState.setBlockAsFirst(false);
            oppState.setFirstBlock(false);
            oppState.setSecondBlock(false);
            oppState.setThirdBlock(false);
        } else if (oppState.getThirdBlock() && !isMy) {
            detectedStrategy = OPP_DUMMY_STRATEGY;
        }
        if (SHOOT.equals(oppMove)) {
            if (oppState.getLastMoveShoot() || !oppState.getLoaded()) {
                if (!isMy) {
                    detectedStrategy = OPP_DUMMY_STRATEGY;
                }
            } else {
                oppState.setLoaded(false);
                oppState.setLastMoveShoot(true);
            }
        } else if (RELOAD.equals(oppMove)) {
            if (oppState.getLastMoveShoot() && myState.getLoaded()) {
                if (isMy) {
                    detectedStrategy = MY_DEFENSIVE_STRATEGY;
                } else if (!OPP_DUMMY_STRATEGY.equals(detectedStrategy) || !OPP_IMPRESIVE_STRATEGY.equals(detectedStrategy)) {
                    detectedStrategy = OPP_OFFENSIVE_STRATEGY;
                }
            }
            if (oppState.getLoaded()) {
                oppState.setDoubleReload(true);
                if (!isMy && !OPP_DUMMY_STRATEGY.equals(detectedStrategy)) {
                    detectedStrategy = OPP_IMPRESIVE_STRATEGY;
                }
            }
            oppState.setLoaded(true);
        } else if (BLOCK.equals(oppMove)) {
            if (!oppState.getBlockAsFirst() && !myState.getBlockAsFirst() && !BLOCK.equals(myMove)) {
                oppState.setBlockAsFirst(true);
            }
            if (oppState.getFirstBlock()) {
                if (!isMy && !OPP_IMPRESIVE_STRATEGY.equals(detectedStrategy) && !OPP_DUMMY_STRATEGY.equals(detectedStrategy)) {
                    detectedStrategy = OPP_SUSTAINABLE_STRATEGY;
                }
                oppState.setFirstBlock(false);
                oppState.setSecondBlock(true);
            } else if (oppState.getSecondBlock()) {
                oppState.setSecondBlock(false);
                oppState.setThirdBlock(true);
                if (!isMy && !OPP_IMPRESIVE_STRATEGY.equals(detectedStrategy)) {
                    detectedStrategy = OPP_DEFENSIVE_STRATEGY;
                }
            } else if (!oppState.getFirstBlock() && !oppState.getSecondBlock() && !oppState.getThirdBlock()) {
                oppState.setFirstBlock(true);
            }
        }
        if (!SHOOT.equals(oppMove)) {
            oppState.setLastMoveShoot(false);
        }

        return new MutablePair<>(oppState, detectedStrategy);
    }

    public void defineStrategy() {
        RetrieveData app = new RetrieveData();
        //app.retrieveStrategy();
    }
}
