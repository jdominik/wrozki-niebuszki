package com.dominik.db;


import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.sql.*;

public class RetrieveData {

    public State retrieveState(Long stateRecordId){
        String sql = "SELECT * FROM State WHERE ID = " + stateRecordId;

        Connect conn = new Connect();
        Connection connection = conn.connect();

        try {

            Statement stmt  = connection.createStatement();
            //stmt.
            //PreparedStatement pstmt = connection.prepareStatement(sql);
            //pstmt.setLong(1, stateRecordId);
            ResultSet rs    = stmt.executeQuery(sql);

            // loop through the result set
            while (rs.next()) {
                System.out.println(rs.getInt("ID") +  "\t" +
                        rs.getString("NAME"));
                return new State(rs.getString("NAME"),
                        rs.getBoolean("LOADED"),
                        rs.getBoolean("FIRST_BLOCK"),
                        rs.getBoolean("SECOND_BLOCK"),
                        rs.getBoolean("THIRD_BLOCK"),
                        rs.getBoolean("LAST_MOVE_SHOOT"),
                        rs.getBoolean("DOUBLE_RELOAD"),
                        rs.getBoolean("NOT_RELOAD_WHEN_DOUBLE_EMPTY"),
                        rs.getBoolean("BLOCK_AS_FIRST"),
                        rs.getBoolean("IS_MY_STATE"),
                        rs.getLong("ID"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return null;
    }

    public void retrieveCurrentStrategy(){
        String sql = "SELECT OPP_STRATEGY,MY_STRATEGY FROM Games WHERE ID = ?";

        Connect conn = new Connect();
        Connection connection = conn.connect();

        try {

            Statement stmt  = connection.createStatement();
            ResultSet rs    = stmt.executeQuery(sql);

            // loop through the result set
            while (rs.next()) {
                System.out.println(rs.getInt("ID") +  "\t" +
                        rs.getString("NAME"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    public Long retrieveMaxGame() {
        String sql = "SELECT MAX(ID_GAME) FROM Game";

        Connect conn = new Connect();
        Connection connection = conn.connect();

        try {

            Statement stmt  = connection.createStatement();
            ResultSet rs    = stmt.executeQuery(sql);

            // loop through the result set
            while (rs.next()) {

                System.out.println(rs.getLong("MAX(ID_GAME)"));
                return rs.getLong("MAX(ID_GAME)");
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return null;
    }

    public Long retrieveMaxState() {
        String sql = "SELECT MAX(ID) FROM State";
        Connect conn = new Connect();
        Connection connection = conn.connect();

        try {

            Statement stmt  = connection.createStatement();
            ResultSet rs    = stmt.executeQuery(sql);

            // loop through the result set
            while (rs.next()) {

                System.out.println(rs.getLong("MAX(ID)"));
                return rs.getLong("MAX(ID)");
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return null;
    }


    public void updateState(String name, Boolean loaded, Boolean firstBlock, Boolean secondBlock, Boolean thirdBlock, Boolean lastMoveShoot,
                             Boolean doubleReload, Boolean notReloadWhenDoubleEmpty, Boolean blockAsFirst, Boolean isMyState, Long stateRecordId) {
        String sqlUpdateState = "UPDATE State SET NAME = ?,LOADED = ?,FIRST_BLOCK = ?,SECOND_BLOCK = ?,THIRD_BLOCK = ?,LAST_MOVE_SHOOT = ?,DOUBLE_RELOAD = ?,NOT_RELOAD_WHEN_DOUBLE_EMPTY = ?,IS_MY_STATE = ?,BLOCK_AS_FIRST = ? WHERE ID = ?";

        Connect conn = new Connect();
        Connection connection = conn.connect();
        try{
            PreparedStatement pstmt = connection.prepareStatement(sqlUpdateState);

            pstmt.setString(1, name);
            pstmt.setBoolean(2, loaded);
            pstmt.setBoolean(3, firstBlock);
            pstmt.setBoolean(4, secondBlock);
            pstmt.setBoolean(5, thirdBlock);
            pstmt.setBoolean(6, lastMoveShoot);
            pstmt.setBoolean(7, doubleReload);
            pstmt.setBoolean(8, notReloadWhenDoubleEmpty);
            pstmt.setBoolean(9, isMyState);
            pstmt.setBoolean(10, blockAsFirst);
            pstmt.setLong(11, stateRecordId);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    public Pair<Long,Long> initializeStates() {
        Long lastStateId = this.retrieveMaxState() +1;
        String sqlMyState = "INSERT INTO State(ID,NAME,LOADED,FIRST_BLOCK,SECOND_BLOCK,THIRD_BLOCK,LAST_MOVE_SHOOT,DOUBLE_RELOAD,NOT_RELOAD_WHEN_DOUBLE_EMPTY,IS_MY_STATE,BLOCK_AS_FIRST) VALUES(?,'START',1,0,0,0,0,0,0,1,0)";
        String sqlOppState = "INSERT INTO State(ID,NAME,LOADED,FIRST_BLOCK,SECOND_BLOCK,THIRD_BLOCK,LAST_MOVE_SHOOT,DOUBLE_RELOAD,NOT_RELOAD_WHEN_DOUBLE_EMPTY,IS_MY_STATE,BLOCK_AS_FIRST) VALUES(?,'START',1,0,0,0,0,0,0,0,0)";

        Connect conn = new Connect();
        Connection connection = conn.connect();
        // mine
        try{

            PreparedStatement pstmt = connection.prepareStatement(sqlMyState);
            pstmt.setLong(1, lastStateId);
/*            pstmt.setString(2, name);
            pstmt.setBoolean(3, loaded);
            pstmt.setBoolean(4, firstBlock);
            pstmt.setBoolean(5, secondBlock);
            pstmt.setBoolean(6, thirdBlock);
            pstmt.setBoolean(7, lastMoveShoot);
            pstmt.setBoolean(8, doubleReload);
            pstmt.setBoolean(9, notReloadWhenDoubleEmpty);
            pstmt.setBoolean(10, isMyState);*/
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
        // opponent
        Connect conn2 = new Connect();
        Connection connection2 = conn.connect();

        try{
            PreparedStatement pstmt = connection2.prepareStatement(sqlOppState);
            pstmt.setLong(1, lastStateId +1);
/*            pstmt.setString(2, name);
            pstmt.setBoolean(3, loaded);
            pstmt.setBoolean(4, firstBlock);
            pstmt.setBoolean(5, secondBlock);
            pstmt.setBoolean(6, thirdBlock);
            pstmt.setBoolean(7, lastMoveShoot);
            pstmt.setBoolean(8, doubleReload);
            pstmt.setBoolean(9, notReloadWhenDoubleEmpty);
            pstmt.setBoolean(10, isMyState);*/
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (connection2 != null) {
                    connection2.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return new MutablePair<>(lastStateId,lastStateId+1);
    }

    public Long retrieveMoveTypeId(String actionMove){
        String sql = "SELECT ID FROM Move WHERE REQ_NAME = '" + actionMove + "'";
        Connect conn = new Connect();
        Connection connection = conn.connect();

        try {

            Statement stmt  = connection.createStatement();
            ResultSet rs    = stmt.executeQuery(sql);

            // loop through the result set
            while (rs.next()) {
                System.out.println(rs.getLong("ID"));
                return (rs.getLong("ID"));

            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return null;
    }
}
