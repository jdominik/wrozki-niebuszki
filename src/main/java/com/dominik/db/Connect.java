package com.dominik.db;

import java.sql.*;

public class Connect {

    private static final String DB_URL = "jdbc:sqlite:GameBlackBox.db";


    /**
     * Connect to a sample database
     */
    public static void testConnect() throws ClassNotFoundException {
        Class.forName("org.sqlite.JDBC");
        Connection conn = null;
        try {
            // db parameters
            // create a connection to the database
            conn = DriverManager.getConnection(DB_URL);

            System.out.println("Connection to SQLite has been established.");

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

        protected Connection connect() {
            // SQLite connection string
            Connection conn = null;
            try {
                conn = DriverManager.getConnection(DB_URL);
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            return conn;
        }


        public void insert(String name, double capacity) {
            String sql = "INSERT INTO employees(name, capacity) VALUES(?,?)";

            try{
                Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql);
                pstmt.setString(1, name);
                pstmt.setDouble(2, capacity);
                pstmt.executeUpdate();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }

    public void selectAll(){
        String sql = "SELECT * FROM employees";

        try {
            Connection conn = this.connect();
            Statement stmt  = conn.createStatement();
            ResultSet rs    = stmt.executeQuery(sql);

            // loop through the result set
            while (rs.next()) {
                System.out.println(rs.getInt("id") +  "\t" +
                        rs.getString("name") + "\t" +
                        rs.getDouble("capacity"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }


    /**
     * @param args the command line arguments
     */
//    public static void main(String[] args) {
//        SelectRecords app = new SelectRecords();
//        app.selectAll();
//    }
//
//    public static void main(String[] args) {
//
//            InsertRecords app = new InsertRecords();
//            // insert three new rows
//            app.insert("Aryan", 30000);
//            app.insert("Robert", 40000);
//            app.insert("Jerry", 50000);
//        }

    //}

}