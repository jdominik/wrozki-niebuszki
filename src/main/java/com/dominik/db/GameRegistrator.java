package com.dominik.db;

import org.apache.commons.lang3.tuple.Pair;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import static com.dominik.logic.DecisionMaker.MY_SUSTAINABLE_STRATEGY;
import static com.dominik.logic.DecisionMaker.OPP_ALTERNATING_STRATEGY;

public class GameRegistrator {

    private final Long gameNo;
    public final Long myStateRecordId;
    public final Long oppStateRecordId;



    private Long myCurrentStrategy;
    private Long oppCurrentStrategy;


    public GameRegistrator() {
        RetrieveData data = new RetrieveData();
        gameNo = data.retrieveMaxGame() + 1;
        moveNo = 1L;
        Pair<Long, Long> statePair = data.initializeStates();
        myStateRecordId = statePair.getKey();
        oppStateRecordId = statePair.getValue();
        initializeStrategy();
    }

    //private static final Long gameNo;
    private static Long moveNo;

    private void initializeStrategy() {
        this.setMyCurrentStrategy(MY_SUSTAINABLE_STRATEGY);
        this.setOppCurrentStrategy(OPP_ALTERNATING_STRATEGY);
    }


    public void insert(Long oppMove,Long oppState,Long myMove,Long myState,Long myLife,Long oppLife) {
        //String sql = "INSERT INTO State(NAME,LOADED,FIRST_BLOCK,SECOND_BLOCK,THIRD_BLOCK,LAST_MOVE_SHOOT,DOUBLE_RELOAD,NOT_RELOAD_WHEN_DOUBLE_EMPTY,IS_MY_STATE) VALUES(?,?,?,?,?,?,?,?,?,?,?)";

        String sqlGame = "INSERT INTO Game(ID_GAME,NO_MOVE,OPP_MOVE,OPP_STATE,OPP_STRATEGY,MY_MOVE,MY_STATE,MY_STRATEGY,MY_LIFE,OPP_LIFE) VALUES(?,?,?,?,?,?,?,?,?,?)";
        Connect conn = new Connect();
        Connection connection = conn.connect();

        try{

            PreparedStatement pstmt = connection.prepareStatement(sqlGame);
            pstmt.setLong(1, gameNo);
            pstmt.setLong(2, moveNo);
            pstmt.setLong(3, oppMove);
            pstmt.setLong(4, oppState);
            pstmt.setLong(5, getOppCurrentStrategy());
            pstmt.setLong(6, myMove);
            pstmt.setLong(7, myState);
            pstmt.setLong(8, getMyCurrentStrategy());
            pstmt.setLong(9, myLife);
            pstmt.setLong(10, oppLife);
            moveNo = moveNo++;
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    public Long getMyCurrentStrategy() {
        return myCurrentStrategy;
    }

    public void setMyCurrentStrategy(Long myCurrentStrategy) {
        this.myCurrentStrategy = myCurrentStrategy;
    }

    public Long getOppCurrentStrategy() {
        return oppCurrentStrategy;
    }

    public void setOppCurrentStrategy(Long oppCurrentStrategy) {
        this.oppCurrentStrategy = oppCurrentStrategy;
    }
}
