package com.dominik.db;

public class State {
    private String name;
    private Boolean loaded;
    private Boolean firstBlock;
    private Boolean secondBlock;
    private Boolean thirdBlock;
    private Boolean lastMoveShoot;
    private Boolean doubleReload;
    private Boolean notReloadWhenDoubleEmpty;
    private Boolean blockAsFirst;
    private Boolean isMyState;
    private Long stateRecordId;

    public State(Long stateRecordId) {
        RetrieveData data = new RetrieveData();
        data.retrieveState(stateRecordId);
    }

    public State(String name, Boolean loaded, Boolean firstBlock, Boolean secondBlock, Boolean thirdBlock, Boolean lastMoveShoot,
                 Boolean doubleReload, Boolean notReloadWhenDoubleEmpty, Boolean blockAsFirst, Boolean isMyState, Long stateRecordId) {
        this.name = name;
        this.loaded = loaded;
        this.firstBlock = firstBlock;
        this.secondBlock = secondBlock;
        this.thirdBlock = thirdBlock;
        this.lastMoveShoot = lastMoveShoot;
        this.doubleReload = doubleReload;
        this.notReloadWhenDoubleEmpty = notReloadWhenDoubleEmpty;
        this.blockAsFirst = blockAsFirst;
        this.isMyState = isMyState;
        this.stateRecordId = stateRecordId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getLoaded() {
        return loaded;
    }

    public void setLoaded(Boolean loaded) {
        this.loaded = loaded;
    }

    public Boolean getFirstBlock() {
        return firstBlock;
    }

    public void setFirstBlock(Boolean firstBlock) {
        this.firstBlock = firstBlock;
    }

    public Boolean getSecondBlock() {
        return secondBlock;
    }

    public void setSecondBlock(Boolean secondBlock) {
        this.secondBlock = secondBlock;
    }

    public Boolean getThirdBlock() {
        return thirdBlock;
    }

    public void setThirdBlock(Boolean thirdBlock) {
        this.thirdBlock = thirdBlock;
    }

    public Boolean getLastMoveShoot() {
        return lastMoveShoot;
    }

    public void setLastMoveShoot(Boolean lastMoveShoot) {
        this.lastMoveShoot = lastMoveShoot;
    }

    public Boolean getDoubleReload() {
        return doubleReload;
    }

    public void setDoubleReload(Boolean doubleReload) {
        this.doubleReload = doubleReload;
    }

    public Boolean getNotReloadWhenDoubleEmpty() {
        return notReloadWhenDoubleEmpty;
    }

    public void setNotReloadWhenDoubleEmpty(Boolean notReloadWhenDoubleEmpty) {
        this.notReloadWhenDoubleEmpty = notReloadWhenDoubleEmpty;
    }

    public Boolean getBlockAsFirst() {
        return blockAsFirst;
    }

    public void setBlockAsFirst(Boolean blockAsFirst) {
        this.blockAsFirst = blockAsFirst;
    }

    public Boolean getMyState() {
        return isMyState;
    }

    public void setMyState(Boolean myState) {
        isMyState = myState;
    }

    public Long getStateRecordId() {
        return stateRecordId;
    }

    public void setStateRecordId(Long stateRecordId) {
        this.stateRecordId = stateRecordId;
    }
}
