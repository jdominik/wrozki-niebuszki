package hello;

public class Request {

    private String game;
    private String playerAction;
    private Long playerLife;
    private String opponentAction;
    private Long opponentLife;
    private String result;

    public String getGame() {
        return game;
    }

    public void setGame(String game) {
        this.game = game;
    }

    public String getPlayerAction() {
        return playerAction;
    }

    public void setPlayerAction(String playerAction) {
        this.playerAction = playerAction;
    }

    public Long getPlayerLife() {
        return playerLife;
    }

    public void setPlayerLife(Long playerLife) {
        this.playerLife = playerLife;
    }

    public String getOpponentAction() {
        return opponentAction;
    }

    public void setOpponentAction(String opponentAction) {
        this.opponentAction = opponentAction;
    }

    public Long getOpponentLife() {
        return opponentLife;
    }

    public void setOpponentLife(Long opponentLife) {
        this.opponentLife = opponentLife;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
