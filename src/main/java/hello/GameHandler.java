package hello;

import com.dominik.db.RetrieveData;
import com.dominik.db.State;
import com.dominik.logic.DecisionMaker;
import org.apache.commons.lang3.tuple.Pair;


public class GameHandler implements Response {

    // handle the game request - generate response
    // “playerAction”: “shoot”,
    // “playerLife”: 3,
    // “opponentAction”: “block”,
    // “opponentLife”: 3,
    // “result”: “not hurt”

    private final String action;


    public GameHandler() {
        this.action = "";
    }

    public GameHandler(String playerAction, Long playerLife, String opponentAction, Long opponentLife, String result) {
        Long myStateRecordId = GameLauncher.gameRegistrator.myStateRecordId;
        Long oppStateRecordId = GameLauncher.gameRegistrator.oppStateRecordId;
        RetrieveData data = new RetrieveData();
        Long oppMove = data.retrieveMoveTypeId(opponentAction);
        Long myMove = data.retrieveMoveTypeId(playerAction);


        GameLauncher.gameRegistrator.insert(oppMove,oppStateRecordId, myMove, myStateRecordId, playerLife, opponentLife);
        DecisionMaker decision = new DecisionMaker();
        Pair<State,Long> oppStateStrategy = decision.defineState(GameLauncher.gameRegistrator.oppStateRecordId, GameLauncher.gameRegistrator.myStateRecordId, oppMove, myMove, GameLauncher.gameRegistrator.getOppCurrentStrategy());
        Pair<State,Long> myStateStrategy = decision.defineState(GameLauncher.gameRegistrator.myStateRecordId, GameLauncher.gameRegistrator.oppStateRecordId, myMove, oppMove, GameLauncher.gameRegistrator.getMyCurrentStrategy());

        GameLauncher.gameRegistrator.setOppCurrentStrategy(oppStateStrategy.getValue());
        GameLauncher.gameRegistrator.setMyCurrentStrategy(myStateStrategy.getValue());

        updateOppState(data, oppStateStrategy.getKey());
        updateMyState(data, myStateStrategy.getKey());
        this.action = decision.takeAction(decision.makeDecision(myStateStrategy.getKey(),oppStateStrategy.getKey(),GameLauncher.gameRegistrator.getMyCurrentStrategy(),GameLauncher.gameRegistrator.getOppCurrentStrategy()));
    }

    protected void updateOppState(RetrieveData data, State oppState) {
        //oppState
        data.updateState(oppState.getName(), oppState.getLoaded(), oppState.getFirstBlock(), oppState.getSecondBlock(), oppState.getThirdBlock(), oppState.getLastMoveShoot(), oppState.getDoubleReload(), oppState.getNotReloadWhenDoubleEmpty(), oppState.getBlockAsFirst(), false, GameLauncher.gameRegistrator.oppStateRecordId);
        //myState
        //data.updateState(name, loaded, firstBlock, secondBlock, thirdBlock, lastMoveShoot, doubleReload, notReloadWhenDoubleEmpty, true, GameLauncher.gameRegistrator.myStateRecordId);
    }

    protected void updateMyState(RetrieveData data, State myState) {
        //oppState
        //data.updateState(name, loaded, firstBlock, secondBlock, thirdBlock, lastMoveShoot, doubleReload, notReloadWhenDoubleEmpty, false, GameLauncher.gameRegistrator.oppStateRecordId);
        //myState
        data.updateState(myState.getName(), myState.getLoaded(), myState.getFirstBlock(), myState.getSecondBlock(), myState.getThirdBlock(), myState.getLastMoveShoot(), myState.getDoubleReload(), myState.getNotReloadWhenDoubleEmpty(), myState.getBlockAsFirst(), true, GameLauncher.gameRegistrator.myStateRecordId);
    }


    public String getAction() {
        return action;
    }
}
