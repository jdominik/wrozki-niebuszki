package hello;

public interface ReqConst {

    public static final String WINNER = "winner";
    public static final String BEGIN = "begin";
    public static final String GAME_OVER = "game over";
}
