package hello;

public class GameRequest implements Response {

    // handle the game
    // “playerAction”: “shoot”,
    // “playerLife”: 3,
    // “opponentAction”: “block”,
    // “opponentLife”: 3,
    // “result”: “not hurt”

    private final String playerAction;
    private final Long playerLife;
    private final String opponentAction;
    private final Long opponentLife;
    private final String result;


    public GameRequest() {
        this.playerAction = "";
        this.playerLife = -1L;
        this.opponentAction = "";
        this.opponentLife = -1L;
        this.result = "";
    }

    public GameRequest(String playerAction, Long playerLife, String opponentAction, Long opponentLife, String result) {
        this.playerAction = playerAction;
        this.playerLife = playerLife;
        this.opponentAction = opponentAction;
        this.opponentLife = opponentLife;
        this.result = result;
    }

    public String getPlayerAction() {
        return playerAction;
    }

    public Long getPlayerLife() {
        return playerLife;
    }

    public String getOpponentAction() {
        return opponentAction;
    }

    public Long getOpponentLife() {
        return opponentLife;
    }

    public String getResult() {
        return result;
    }
}
