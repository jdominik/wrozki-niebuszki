package hello;

import com.dominik.db.Connect;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.stereotype.Controller;

@Controller
@SpringBootApplication
public class Application extends SpringBootServletInitializer {

    public final Connect connect = new Connect();




    public static void main(String[] args) {
        Connect connection = new Connect();
        try {
            connection.testConnect();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        SpringApplication.run(Application.class, args);
        //Connect conn = new Connect();
    }


}
