package hello;

import com.dominik.db.GameRegistrator;
import com.dominik.db.RetrieveData;
import com.dominik.db.State;
import com.dominik.logic.DecisionMaker;

import static hello.ReqConst.*;

public class GameLauncher implements Response {


    private final String action;
    protected static GameRegistrator gameRegistrator;

    public GameLauncher() {
        this.action = "";
    }

    public GameLauncher(String gameReq) {
        String response = new String();
        if (BEGIN.equals(gameReq)) {
            System.out.println("==== Starting the tournament ====");
            gameRegistrator = new GameRegistrator();
            DecisionMaker decision = new DecisionMaker();
            RetrieveData data = new RetrieveData();
            State myState = data.retrieveState(gameRegistrator.myStateRecordId);
            State oppState = data.retrieveState(gameRegistrator.oppStateRecordId);
            this.action = decision.takeAction(decision.makeDecision(myState, oppState, GameLauncher.gameRegistrator.getMyCurrentStrategy(), GameLauncher.gameRegistrator.getOppCurrentStrategy()));
        } else {
            this.action = null;
        }
    }


/*    public String getName() {
        if (getAction() == null) {
            return name;
        }
        return null;
    }

    public String getEndpoint() {
        if (getAction() == null) {
            return endpoint;
        }
        return null;
    }*/

    public String getAction() {
        return action;
    }
}
