package hello;

import static hello.ReqConst.GAME_OVER;
import static hello.ReqConst.WINNER;

public class GameBreaker extends GameLauncher {

    private final String name = "Wrozki Niebuszki";
    private final String endpoint = "https://wrozki-niebuszki.herokuapp.com";

    public GameBreaker(String gameReq) {

        if (WINNER.equals(gameReq)) {
            System.out.println("==== Fairy tales are rescued ====");
            gameRegistrator = null;
            //this.action = null;
        } else if (GAME_OVER.equals(gameReq)) {
            System.out.println("==== Fairy tales kingdom - no signal ====");
            gameRegistrator = null;
            //action = null;
        } else {
            System.out.println("==== Wrong request ====");
            //action = null;
        }
    }

    public String getEndpoint() {
        return endpoint;
    }

    public String getName() {
        return name;
    }
}
