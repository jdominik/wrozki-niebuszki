package hello;

import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicLong;

import static hello.ReqConst.BEGIN;

@RestController
@RequestMapping(value = "/", method = RequestMethod.POST)
@ResponseBody
public class GameController {

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();



    // begins or ends the game
    // “game”: “begin”
    // “game”: “winner”
    // “game”: “game over”
    //@CrossOrigin(value ="Wrozki Niebuszki", origins = "http://localhost:9000")
    @CrossOrigin(origins = "https://wrozki-niebuszki.herokuapp.com")
    @PostMapping(path = "/", consumes = "application/json", produces = "application/json")
    public Response launch(@RequestBody(required=false) Request requestBody) throws SQLException {
        Request request = new Request();
        BeanUtils.copyProperties(requestBody, request);
        String game = request.getGame();
        if (BEGIN.equals(game)) {
            return new GameLauncher(game);
        } else if(game != null) {
            return new GameBreaker(request.getGame());
        } else {
            System.out.println("==== turn response ====");
            return new GameHandler(request.getPlayerAction(), request.getPlayerLife(), request.getOpponentAction(),
                    request.getOpponentLife(), request.getResult());
            //return new Game(counter.incrementAndGet(), String.format(template, playerAction));
        }

        //if (game)
        //System.out.println("==== in greeting ====");
        //return new Greeting(counter.incrementAndGet(), String.format(template, game));
    }

    // handle the game
    // “playerAction”: “shoot”,
    // “playerLife”: 3,
    // “opponentAction”: “block”,
    // “opponentLife”: 3,
    // “result”: “not hurt”
/*    @CrossOrigin(origins = "http://localhost:9000")
    @GetMapping("/wrozki2")
    public GameHandler enjoy(@RequestParam(required=false, defaultValue="World") String playerAction, Long playerLife, String opponentAction,
    Long opponentLife, String result) throws SQLException {
        System.out.println("==== in greeting ====");
        return new GameHandler(counter.incrementAndGet(), String.format(template, playerAction));
    }

    @GetMapping("/wrozki-javaconfig")
    public GameHandler greetingWithJavaconfig(@RequestParam(required=false, defaultValue="World") String name) {
        System.out.println("==== in greeting ====");
        return new GameHandler(counter.incrementAndGet(), String.format(template, name));
    }*/

}
